# Copyright 2017-2019 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user='flatpak' release=${PV} suffix='tar.xz' ]
require meson
require systemd-service test-dbus-daemon

export_exlib_phases src_prepare src_test

SUMMARY="A portal frontend service for Flatpak"
DESCRIPTION="
xdg-desktop-portal works by exposing a series of D-Bus interfaces known as
portals under a well-known name and object path. The portal interfaces include
APIs for file access, opening URIs, printing and others.
Flatpak grants sandboxed applications talk access to names in the
org.freedesktop.portal.* prefix. One possible way to use the portal APIs is
thus just to make D-Bus calls. For many of the portals, toolkits (e.g. GTK+)
are expected to support portals transparently if you use suitable high-level
APIs.
To actually use most portals, xdg-desktop-portal relies on a backend that
provides implementations of the org.freedesktop.impl.portal.* interfaces. One
such backend is provided by xdg-desktop-portal-gtk. Another one is in
development here: xdg-desktop-portal-kde.
"

# xdg-desktop-portal needs data/org.freedesktop.portal.Flatpak.xml provided by flatpak at build
# time, but we don't want to pull in all of flatpak as a build dependency.
# This flatpak version should be bumped whenever the data file is updated.
FLATPAK_PV="1.12.0"
FLATPAK_PNV="flatpak-${FLATPAK_PV}"
DOWNLOADS+=" https://github.com/flatpak/flatpak/releases/download/${FLATPAK_PV}/${FLATPAK_PNV}.tar.xz"

LICENCES="LGPL-2.1"
SLOT="0"
MYOPTIONS="
    geolocation [[ description = [ Support for location portal (needs geoclue) ] ]]
    systemd
"

DEPENDENCIES="
    build:
        app-text/docbook-xml-dtd:4.3
        app-text/xmlto
        dev-python/docutils
        sys-devel/gettext[>=0.18.3]
        virtual/pkg-config[>=0.24]
    build+run:
        core/json-glib
        dev-libs/glib:2[>=2.66]
        media/pipewire[>=0.2.90]
        !sys-apps/flatpak[<0.11] [[
            description = [ file collisions, document portal was moved into xdg-desktop-portal ]
            resolution = upgrade-blocked-before
        ]]
        sys-fs/fuse:3[>=3.10.0]
        x11-libs/gdk-pixbuf:2.0
        geolocation? ( gps/geoclue:2.0[>=2.5.2] )
        systemd? ( sys-apps/systemd )
    run:
        sys-apps/bubblewrap
    test:
        sys-libs/libportal
    suggestion:
        kde/xdg-desktop-portal-kde  [[
            description = [ Backend providing integration with the Plasma desktop ]
        ]]
        sys-apps/rtkit [[
            description = [ Provides the backend used by the Realtime portal ]
        ]]
        sys-apps/xdg-desktop-portal-gnome [[
            description = [ Backend providing integration with GNOME ]
        ]]
        sys-apps/xdg-desktop-portal-wlr [[
            description = [ Backend providing integration with wlroots-based WMs such as sway ]
        ]]
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Ddocbook-docs=enabled
    -Dflatpak-interfaces=enabled
    -Dflatpak-interfaces-dir="${WORKBASE}/${FLATPAK_PNV}/data"
    -Dinstalled-tests=false
    -Dman-pages=enabled
    -Dpytest=disabled
    -Dsandboxed-image-validation=true
    -Dsystemd-user-unit-dir="${SYSTEMDUSERUNITDIR}"
)
MESON_SRC_CONFIGURE_OPTION_FEATURES=(
    'geolocation geoclue'
    systemd
)
MESON_SRC_CONFIGURE_TESTS=(
    '-Dlibportal=enabled -Dlibportal=disabled'
)

xdg-desktop-portal_src_prepare() {
    meson_src_prepare

    edo sed \
        -e "/docs_dir =/s:/ meson.project_name():/ '${PNVR}':" \
        -i meson.build

    # test-portals-openuri is sensitive to locally configured mime type bindings
    edo sed -e "/'openuri',/d" -i tests/meson.build
}

xdg-desktop-portal_src_test() {
    esandbox addfilter_path /dev/fuse
    esandbox addfilter_net unix:**/pipewire-0
    esandbox addfilter_net unix:/run/dbus/system_bus_socket

    test-dbus-daemon_run-tests meson_src_test

    esandbox rmfilter_net unix:/run/dbus/system_bus_socket
    esandbox rmfilter_net unix:**/pipewire-0
    esandbox rmfilter_path /dev/fuse
}

