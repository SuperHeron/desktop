# Copyright 2011-2018 Hong Hao <oahong@gmail.com>
# Distributed under the terms of the GNU General Public License v2

require gitlab [ prefix='https://dev.gajim.org' user=gajim ]
require gtk-icon-cache py-pep517 [ backend=setuptools ]

SUMMARY="A full featured and easy to use jabber client"
HOMEPAGE="http://www.gajim.org"
DOWNLOADS="${HOMEPAGE}/downloads/$(ever range -2)/${PNV}.tar.gz"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build:
        virtual/pkg-config
        dev-util/intltool
        sys-devel/gettext
    build+run:
        dev-python/cryptography[>=3.4.8][python_abis:*(-)?]
        dev-python/css-parser[python_abis:*(-)?]
        dev-python/emoji[>=2.6.0][python_abis:*(-)?]
        dev-python/packaging[python_abis:*(-)?]
        dev-python/Pillow[>=9.1.0][python_abis:*(-)?]
        dev-python/precis_i18n[>=1.0.0][python_abis:*(-)?]
        dev-python/pyasn1[python_abis:*(-)?]
        dev-python/pycairo[>=1.16.0][python_abis:*(-)?]
        dev-python/python-nbxmpp[>=5.0.1&<6.0.0][python_abis:*(-)?]
        dev-python/omemo-dr[>=1.0.0&<2.0.0][python_abis:*(-)?]
        dev-python/qrcode[>=7.3.1][python_abis:*(-)?]
        dev-python/SQLAlchemy[>=2.0.0][python_abis:*(-)?]
        gnome-bindings/pygobject:3[cairo][>=3.42.0][python_abis:*(-)?]
        gnome-desktop/gtksourceview:4.0
    recommendation:
        dev-python/dbus-python[>=1.2.0][python_abis:*(-)?] [[ note = [ gajim-remote ] ]]
        (
            dev-libs/libsecret:1[gobject-introspection]
            dev-python/keyring[python_abis:*(-)?]
        ) [[ note = [ password storage ] ]]
"

# Tests require access to X server
RESTRICT="test"

compile_one_multibuild() {
    edo ${PYTHON} pep517build/build_metadata.py -o dist/metadata
    py-pep517_compile_one_multibuild
}

install_one_multibuild() {
    py-pep517_install_one_multibuild
    edo ${PYTHON} pep517build/install_metadata.py dist/metadata --prefix="${IMAGE}"/usr
}

