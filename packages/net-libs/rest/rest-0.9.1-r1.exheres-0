# Copyright 2011 Niki Guldbrand <niki.guldbrand@gmail.com>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ]
require vala [ with_opt=true ]
require meson

SUMMARY="Easy access to RESTful interfaces"
DESCRIPTION="
This library has been designed to make it easier to access web services that
claim to be \"RESTful\".
"
HOMEPAGE="https://wiki.gnome.org/Projects/Librest"

LICENCES="LGPL-2.1"
SLOT="1.0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    gobject-introspection
    gtk-doc [[ requires = [ gobject-introspection ] ]]
    vapi [[ requires = [ gobject-introspection ] ]]
    (
        providers:
            soup2
            soup3
    ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=0.6.7] )
        gtk-doc? ( dev-doc/gi-docgen[>=2021.6] )
    build+run:
        core/json-glib[gobject-introspection?]
        dev-libs/glib:2[>=2.44][gobject-introspection(+)?]
        dev-libs/libxml2:2.0
        providers:soup2? ( gnome-desktop/libsoup:2.4[>=2.42][gobject-introspection] )
        providers:soup3? ( gnome-desktop/libsoup:3.0[>=2.99.2][gobject-introspection] )
"

# needs 0.0.0.0 whitelisting and more
RESTRICT="test"

MESON_SRC_CONFIGURE_PARAMS=(
    -Dca_certificates=true
    -Dca_certificates_path=/etc/ssl/certs/ca-certificates.crt
    -Dexamples=false
)
MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'gobject-introspection introspection'
    'gtk-doc gtk_doc'
    'providers:soup2'
)
MESON_SRC_CONFIGURE_TESTS=(
    '-Dtests=true -Dtests=false'
)

pkg_setup() {
    meson_pkg_setup
    vala_pkg_setup
}

